use actix_web::middleware;
use actix_web::{get, web, App, HttpServer, Responder, Result};
use log::{debug, error, info, warn};
//extern crate paho_mqtt as mqtt;
//use anyhow;
use clap::Parser;
use paho_mqtt as mqtt;
use serde::{Deserialize, Serialize};
//use serde_json;
use std::error::Error;
use std::path::PathBuf;
use std::time::{Duration, SystemTime};

#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
struct Cli {
    /// Sets a custom config file
    #[clap(
        short,
        long,
        value_name = "FILE",
        default_value = "/usr/local/etc/cargateproxy.conf"
    )]
    config: PathBuf,
}

#[derive(Debug, Deserialize, Clone)]
struct Config {
    access: Access,
    bind: String,
    port: u16,
    mqtt_broker_uri: String,
    mqtt_topic: String,
    mqtt_username: Option<String>,
    mqtt_password: Option<String>,
}

#[derive(Clone, Debug, Deserialize)]
struct Access {
    tokenlist: Vec<String>,
}
#[derive(Clone, Debug, Deserialize)]
struct OpenRequest {
    token: String,
}
#[derive(Debug, Serialize)]
struct OpenResponse {
    msg: String,
    error: Option<String>,
    timestamp: u64,
}
#[derive(Clone)]
struct AppState {
    broker: mqtt::Client,
}

#[derive(Debug, Serialize, Deserialize)]
enum GateMessage {
    OPEN,
    CLOSE,
    STATUS,
}
#[derive(Debug, Serialize, Deserialize)]
struct MqttMessage {
    operation: GateMessage,
    token: String,
    timestamp: u64,
}

impl AppState {
    fn new(
        mqtt_server_uri: String,
        mqtt_user: Option<String>,
        mqtt_password: Option<String>,
    ) -> AppState {
        let opts = paho_mqtt::CreateOptionsBuilder::new()
            .server_uri(mqtt_server_uri)
            .client_id("cargateproxy")
            .mqtt_version(paho_mqtt::MQTT_VERSION_5)
            .persistence(None)
            .finalize();

        let mut cli = mqtt::Client::new(opts).unwrap();
        let conn_opts = match mqtt_user {
            Some(user) => paho_mqtt::ConnectOptionsBuilder::new()
                .user_name(user)
                .password(mqtt_password.unwrap())
                .keep_alive_interval(Duration::from_secs(60))
                .automatic_reconnect(Duration::from_secs(2), Duration::from_secs(10))
                .clean_session(true)
                .finalize(),
            None => mqtt::ConnectOptions::new(),
        };
        // opcja jak jest username:

        cli.set_timeout(Duration::from_secs(2));
        if let Err(e) = cli.connect(conn_opts) {
            warn!("MQTT connection error: {}", e);
        }
        AppState { broker: cli }
    }
}

fn read_config(p: &PathBuf) -> std::io::Result<Config> {
    let content = std::fs::read_to_string(p)?;
    Ok(toml::from_str(&content)?)
}
fn timestamp() -> u64 {
    SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap()
        .as_secs()
}
#[get("/gate_open")]
async fn gate_open(
    aps: web::Data<Config>,
    mq: web::Data<AppState>,
    open_r: web::Query<OpenRequest>,
) -> Result<impl Responder> {
    //HttpResponse::Ok().body("Hello world!")
    if aps.access.tokenlist.contains(&open_r.token) {
        //        let message = MqttMessage {
        //            token: open_r.token.clone(),
        //            operation: GateMessage::OPEN,
        //            timestamp: timestamp(),
        //        };
        //        //let json_msg = serde_json::to_string(&message).unwrap();
        let msg = mqtt::MessageBuilder::new()
            .topic(aps.mqtt_topic.clone())
            .payload("blink")
            .qos(1)
            .finalize();
        if !mq.broker.is_connected() {
            info!("MQTT disconnected - reconnecting");
            if let Err(e) = mq.broker.reconnect() {
                warn!("MQTT reconnect failed:{}", e);
            }
        }
        if let Err(e) = mq.broker.publish(msg) {
            warn!("Error sending message: {:?}", e);
            let response = OpenResponse {
                msg: format!("Error serving MQTT request"),
                error: Some(e.to_string()),
                timestamp: timestamp(),
            };
            Ok(web::Json(response))
        } else {
            let response = OpenResponse {
                msg: format!("Request accepted"),
                error: None,
                timestamp: timestamp(),
            };
            Ok(web::Json(response))
        }
    } else {
        let response = OpenResponse {
            msg: format!("Request denied"),
            error: Some("Invalid token".to_string()),
            timestamp: timestamp(),
        };
        Ok(web::Json(response))
    }
}

#[actix_web::main]
async fn main() -> Result<(), Box<dyn Error>> {
    env_logger::init();
    let cli = Cli::parse();
    info!("Parsing config");
    let cfg = read_config(&cli.config).unwrap_or_else(|e| {
        error!("Error reading config: {}", e);
        std::process::exit(-1);
    });
    let bind = cfg.bind.clone();
    let port = cfg.port;
    let mqtt_uri = cfg.mqtt_broker_uri.clone();
    debug!("Config: {:?}", cfg);
    debug!("Preparing MQTT session");
    let mqtt_username = cfg.mqtt_username.clone();
    let app_state = match mqtt_username {
        Some(username) => {
            if username.len() > 0 {
                let mqtt_username = cfg.mqtt_username.clone();
                let mqtt_password = cfg.mqtt_password.clone();
                AppState::new(mqtt_uri, mqtt_username, mqtt_password)
            } else {
                AppState::new(mqtt_uri, None, None)
            }
        }
        None => AppState::new(mqtt_uri, None, None),
    };

    debug!("Starting cargateproxy");
    let _ = HttpServer::new(move || {
        App::new()
            .wrap(middleware::Logger::default())
            .app_data(web::Data::new(cfg.clone()))
            .app_data(web::Data::new(app_state.clone()))
            .service(gate_open)
    })
    .bind((bind, port))
    .unwrap_or_else(|e| {
        error!("Error starting web service: {}", e);
        std::process::exit(-2);
    })
    .run()
    .await;
    Ok(())
}
